package zadania.zadanie_enum;

public enum Kolor {
    CZERWONY(1),
    NIEBIESKI(2),
    ZIELONY(3),
    ŻÓŁTY(4),
    FIOLETOWY(5);

    private int numerKoloru;

    Kolor(int wartosc) {
        this.numerKoloru = wartosc;
    }

    public int getNumerKoloru() {
        return numerKoloru;
    }
}
