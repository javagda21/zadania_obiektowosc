package zadania.zadanie_osoba;

import java.util.Objects;

public class Osoba {
    String imie;
    boolean jestKobieta;
    int rokUrodzenia;


    public Osoba() {
    }

    public Osoba(String imie, int rokUrodzenia) {
        this.imie = imie;
        this.rokUrodzenia = rokUrodzenia;
    }

    public Osoba(String imie, boolean jestKobieta, int rokUrodzenia) {
        this.imie = imie;
        this.jestKobieta = jestKobieta;
        this.rokUrodzenia = rokUrodzenia;
    }

    void przedstawSie() {
        System.out.print("Cześć! Mam na imie " + imie + " mam " + (2019 - rokUrodzenia) + " lat ");
        if (jestKobieta) {
            System.out.println("i jestem kobieta");
        } else {
            System.out.println("i jestem mężczyzną");
        }
    }

    @Override
    public String toString() {
        return "Osoba{" +
                "imie='" + imie + '\'' +
                ", jestKobieta=" + jestKobieta +
                ", rokUrodzenia=" + rokUrodzenia +
                '}';
    }
}
