package zadania.zadanie_osoba;

public class Main {
    public static void main(String[] args) {
        Osoba osobaAnia = new Osoba("Ania", true, 1994);
        // moja pierwsza instancja obiektu Osoba
//        osobaAnia.imie = "Ania";
//        osobaAnia.rokUrodzenia = 1994;
        osobaAnia.przedstawSie();

        Osoba drugaOsoba = new Osoba("Andrzej", false, 1965);
//        drugaOsoba.imie = "Andrzej";
//        drugaOsoba.rokUrodzenia = 1965;
        drugaOsoba.przedstawSie();

        Osoba trzeciaOsoba = new Osoba("Mariola", false, 1951);
//        trzeciaOsoba.imie = "Mariola";
//        trzeciaOsoba.rokUrodzenia = 1951;
        trzeciaOsoba.przedstawSie();

        Osoba[] tablicaOsob = new Osoba[3];
        tablicaOsob[0] = osobaAnia;
        tablicaOsob[1] = drugaOsoba;
        tablicaOsob[2] = trzeciaOsoba;

        for (int i = 0; i < tablicaOsob.length; i++) {
            // każda osoba przedstawia się
            tablicaOsob[i].przedstawSie();
        }

        // pętla foreach
        // dla każdego obiektu typu ... (klasa)
        // o nazwie ... (nazwa zmiennej)
        // z kolekcji ... (nazwa kolekcji)
        for (Osoba osoba : tablicaOsob) {
            osoba.przedstawSie();
        }

        // wypisz tylko kobiety:
        System.out.println("Kobiety:");
        for (Osoba osoba : tablicaOsob) {
            if (osoba.jestKobieta) {
                osoba.przedstawSie();
            }
        }

        // wypisz tylko mężczyzn
        System.out.println("Mężczyźni:");
        for (Osoba osoba : tablicaOsob) {
            if (!osoba.jestKobieta) {
                osoba.przedstawSie();
            }
        }

        System.out.println();

        System.out.println(osobaAnia.toString());
        System.out.println(drugaOsoba);
        System.out.println(trzeciaOsoba);

    }
}
