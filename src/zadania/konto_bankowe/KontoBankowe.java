package zadania.konto_bankowe;

public class KontoBankowe {
    private Long numerKonta;
    private int stanKonta;

    public KontoBankowe(Long numerKonta, int stanKonta) {
        this.numerKonta = numerKonta;
        this.stanKonta = stanKonta;
    }

    public Long getNumerKonta() {
        return numerKonta;
    }

    public int getStanKonta() {
        return stanKonta;
    }

    void wyswietlStanKonta() {
        System.out.println("Stan konta: " + stanKonta);
    }

    void wplacSrodki(int ileWplacic) {
        stanKonta += ileWplacic;
    }

    int pobierzSrodki(int ilePobrac) {
        if (ilePobrac <= stanKonta) {
            // jeśli mamy tyle pieniążków
            // to dokładnie tyle wypłacamy
            stanKonta -= ilePobrac;
            return ilePobrac;
        }
        int ileMamy = stanKonta;

        stanKonta = 0;
        return ileMamy;
        // nie starczyło nam środków,
        // zwracamy tyle ile było na koncie
    }


}
