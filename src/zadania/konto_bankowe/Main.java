package zadania.konto_bankowe;

public class Main {
    public static void main(String[] args) {
        KontoBankowe kontoAndrzeja = new KontoBankowe(555L, 1000);
//        kontoAndrzeja.numerKonta = 555L;
//        kontoAndrzeja.stanKonta = 1000;

        KontoBankowe kontoBeaty = new KontoBankowe(666L, 2000);
//        kontoBeaty.numerKonta = 666L;
//        kontoBeaty.stanKonta = 2000;

        // zdefiniowałem sobie zmienną kwota przelewu aby operować tą kwotą
        int kwotaPrzelewu = 300;

        // 1 - 300
        // 2 - 300
        // 3 - 300
        // 4 - 100
        // 5 - 0
        for (int i = 0; i < 5; i++) {
            // pobieram środki z konta andrzeja.
            int pobraneSrodki = kontoAndrzeja.pobierzSrodki(kwotaPrzelewu);

            // wpłacam środki na konto beaty.
            kontoBeaty.wplacSrodki(pobraneSrodki);

            kontoAndrzeja.wyswietlStanKonta();
            kontoBeaty.wyswietlStanKonta();
            System.out.println();
        }
    }
}
