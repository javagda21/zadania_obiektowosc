package zadania.zadanie_bilet;

import zadania.zadanie_superbohater.SuperBohater;

public class Main {
    public static void main(String[] args) {
        Bilet bilet = Bilet.NORMALNY_CAŁODOBOWY;
        Bilet bilet_2 = Bilet.ULGOWY_CAŁODOBOWY;

        System.out.println(bilet);
        bilet.wyswietlDaneOBilecie();

        System.out.println(bilet_2);
        bilet_2.wyswietlDaneOBilecie();
    }
}
