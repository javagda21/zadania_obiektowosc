package zadania.zadanie_bilet;

public enum Bilet {
    ULGOWY_GODZINNY(1.6, 60),
    NORMALNY_GODZINNY(3.2, 60),
    ULGOWY_CAŁODOBOWY(5.0, 1440),
    NORMALNY_CAŁODOBOWY(10, 1440),
    BRAK_BILETU(10000000.0, 0);

    private double cena;
    private int czasJazdy;

    Bilet(double cena, int czasJazdy) {
        this.cena = cena;
        this.czasJazdy = czasJazdy;
    }

    public double pobierzCeneBiletu() {
        return cena;
    }

    public int pobierzCzasJazdy() {
        return czasJazdy;
    }

    public void wyswietlDaneOBilecie() {
        switch (this) {
            case BRAK_BILETU:
                System.out.println("Brak biletu.");
                break;
            case ULGOWY_GODZINNY:
                System.out.println("Bilet ulgowy " + czasJazdy + " minutowy");
                break;
            case NORMALNY_GODZINNY:
                System.out.println("Bilet normalny " + czasJazdy + " minutowy");
                break;
            case ULGOWY_CAŁODOBOWY:
                System.out.println("Bilet ulgowy " + czasJazdy + " minutowy");
                break;
            case NORMALNY_CAŁODOBOWY:
                System.out.println("Bilet normalny " + czasJazdy + " minutowy");
                break;
        }
    }
}
