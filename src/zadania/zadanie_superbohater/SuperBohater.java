package zadania.zadanie_superbohater;

import java.util.Objects;

public class SuperBohater {
    private String nazwa;
    private String supermoc;

    public SuperBohater(String nazwa, String supermoc) {
        this.nazwa = nazwa;
        this.supermoc = supermoc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SuperBohater bohater = (SuperBohater) o;
        return Objects.equals(getNazwa(), bohater.getNazwa());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getNazwa());
    }

    public void podajImie(){
        System.out.println("Moje imie to: " + this.nazwa);
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getSupermoc() {
        return supermoc;
    }

    public void setSupermoc(String supermoc) {
        this.supermoc = supermoc;
    }
}
