package zadania.zadanie_superbohater;

public class Main {
    public static void main(String[] args) {
        SuperBohater bohater1 = new SuperBohater("Marian", "Prowadzenie po pijaku");
        SuperBohater bohater2 = new SuperBohater("Bogdan", "Przekonywanie żony");
        SuperBohater bohater3 = new SuperBohater("Paweł", "Tłumaczenie się");

        if (bohater1 == bohater2) {

        }else{
            System.out.println("bohater1 == bohater2 - false");
        }

        if (bohater1.equals(bohater2)) {
            // false
        }else{
            System.out.println("bohater1.equals(bohater2) - false");
        }


        SuperBohater bohater4 = new SuperBohater("Marian", "Prowadzenie po pijaku");
        if (bohater1 == bohater4) {
            // false
        }else{
            System.out.println("bohater1 == bohater4 - false");
        }
        if (bohater1.equals(bohater4)) {
            System.out.println("bohater1.equals(bohater4) - true - tylko jeśli nadpiszemy metodę 'equals' w klasie SuperBohater");
        }
    }
}
